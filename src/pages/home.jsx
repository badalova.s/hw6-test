
import PropTypes from 'prop-types';
import List from "../components/list"
import Header from "../header";
import { useEffect, useState } from 'react';
import { ThemeProvider } from '../context/sumContext';

export default function Home ({cart}) {


          return (
 <ThemeProvider>
          <List cart={cart} /> 

          </ThemeProvider>
          );
        
         
    

}
  
      