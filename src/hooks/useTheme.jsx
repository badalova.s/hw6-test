import { useContext } from "react"
import { ThemContext } from "../context/sumContext"

const useTheme=()=>{
    const value = useContext(ThemContext)
    return value
}
export default useTheme