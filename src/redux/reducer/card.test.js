import { cardsReducer } from "./card";

import React from "react";

test("card state",()=> {
    expect(cardsReducer(undefined,{type:undefined})).toEqual({cartItems:[]})
})

test('add card', () => {
    const action = {type: "ADD_CART",payload:[1,2]}
    const result = cardsReducer([], action)
    expect(result).toMatchObject( {"cartItems": [[1, 2]]})
    })
test('add buy card', () => {
    const cart = [{id:1}]
    const action = {type: "ADD_BUY"}
    const result = cardsReducer(cart, action)
    expect(result).toEqual({"cartItems": []})
})
