import { modalReducer } from "./modal";

import { cleanup } from "@testing-library/react";

afterEach(cleanup);

test('state Modal', ()=> {

    expect(modalReducer(undefined,{type:undefined})).toEqual({modal:false})
})
