import { listsReducer } from "../reducer/lists";

import { cleanup } from "@testing-library/react";

afterEach(cleanup);

const initialState = [];

const list = [{"id":"1","name":"Ноутбук Acer Aspire 3 A315-58-354Q","price":"15 499₴","image":"/1.jpg","color":"Pure Silver","article":"fd5643466"}
,{"id":"2","name":"Ноутбук ASUS TUF Gaming F15 FX506HM-HN017 (90NR0753-M01170) ","price":"45 999₴","image":"/2.jpg","color":"Eclipse Gray","article":"6fgh464346"}
,{"id":"3","name":"Ноутбук Acer Aspire 5 A515-45-R2ZN (NX.A7ZEU.002)","price":"15 499₴","image":"/3.jpg","color":"Charcoal Black","article":"f5fd5643466"}
]


test('list state', () => {
    const result = listsReducer(undefined, { type : ''})
    expect(result).toEqual(initialState)
})

test('add list product', () => {
    const action = {type: 'FILL_LISTS', payload: list}
    const result = listsReducer(initialState, action)
    expect(result).toEqual(list)   
})
