import { favoriteReducer } from "./favorite";

import React from "react";

test("favorite state",()=> {
    expect(favoriteReducer(undefined,{type:undefined})).toEqual({favoriteItems:[]})
})

test('add favorite', () => {
    const action = {type: "ADD_FAVORITE",payload:[1,2]}
    const result = favoriteReducer([], action)
    expect(result).toMatchObject( {"favoriteItems": [[1, 2]]})
    })
