import {createContext, useState} from "react";
import { useMemo } from "react";
export const ThemContext = createContext({isTable:'false'})


export const ThemeProvider=({children})=>{
    const [isTable,setIsTable]=useState('false')
    const value =useMemo(()=>({isTable,setIsTable}),[isTable])
    return <ThemContext.Provider value={value}>{children}</ThemContext.Provider>
}