import { Component, useContext } from "react";
import PropTypes from 'prop-types';
import Card from "../card"
import Modal from "../modal"
import "./list.scss"
import { useEffect, useState } from "react";
import ButtonAdd from "../button_add"
import ButtonFav from "../button_fav"
import {useDispatch} from "react-redux";
import {toggleModal} from "../../redux/actions/modal";
import { connect } from 'react-redux';
import { useSelector } from "react-redux";
import { addCart } from "../../redux/actions/card";
import { addFavorite } from "../../redux/actions/favorite";
import { deleteFavorite } from "../../redux/actions/favorite";
import { ThemContext } from "../../context/sumContext";
import useTheme from "../../hooks/useTheme";




function List({cart,addCart,deleteFavorite,addFavorite,children}) {

const {isTable,setIsTable}=useTheme()
  let [its,setIt]=useState(null)
  const dispatch = useDispatch();
  const listproduct = useSelector((state) => state.lists);
  if (listproduct==null) {
    return <div>loading...</div>
  } 
  else
  return (
    <>
        <button className="list__button" onClick={
      () =>setIsTable(!isTable) }>TOOGLE VIEV</button>

   <ul className={isTable? "list table" : "list"}>
      { listproduct.map((el)=> (
       <li className="list__card" key={el.id}>
          <Card datatestid={`list-${el.id}`} key={el.id} name={el.name} image={el.image} 
            id={el.id}
            price={el.price}
            color={el.color}
            article={el.article}/>
            
           <div className="card__button">
           <ButtonAdd Click={()=>{
           dispatch(toggleModal())
           setIt(el) 
           }
           }/>
           <ButtonFav addDelete={(e)=>{
          if(e.target.parentNode.classList.contains('active')){
            e.target.parentNode.classList.remove('active')
            deleteFavorite(el.id)
        }else {
          e.target.parentNode.classList.add('active');
          addFavorite(el)
        }  
          }}
/>
                      </div>
                       </li>
                     ))}
                     
                      
                    
                    </ul>

                    <Modal
          question={"Ви хочете додати цей товар до корзини"} 
          add={(e)=>{
            addCart(its) 
            dispatch(toggleModal())         
            }}/>          
            
                   </>

   
)
}
const mapDispatchToProps = dispatch => ({
  addCart: item => dispatch(addCart(item)),
  addFavorite: item => dispatch(addFavorite(item)),
  deleteFavorite: id => dispatch(deleteFavorite(id))
})
export default connect(null, mapDispatchToProps)(List)