import ButtonAdd from '../button_add';
import { render, fireEvent, cleanup, screen } from '@testing-library/react';
import renderer from "react-test-renderer";
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup)


const onClickMock = jest.fn()


test('test ButtonAdd props', () => {
    const props = {
        text:"ADD TO CART",
        classname:"button",
        title: 'tests title',
        onClick : onClickMock,
        
        }
    render(<ButtonAdd {...props} />);
    expect(screen.getByText('ADD TO CART')).toBeInTheDocument();

  })

  test('onClick ButtonAdd', () => {
    const props = {
        text:"ADD TO CART",
        classname:"button",
        onClick : onClickMock(),
        
        }
    render(<ButtonAdd {...props} />)

    const button = screen.getByTestId('btn');
    fireEvent.click(button)
    expect(onClickMock).toHaveBeenCalled()
  })

test('snapshot ButtonAdd', () => {
    const btnProps = {
        text:"ADD TO CART",
        classname:"button",
        onClick : onClickMock,
  }
    const button = renderer.create(<ButtonAdd {...btnProps}/>).toJSON;
    expect(button).toMatchSnapshot()
});

