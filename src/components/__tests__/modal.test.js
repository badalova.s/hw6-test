import { render, screen, fireEvent, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import Modal from "../modal";
import configureStore from 'redux-mock-store';
import { toggleModal } from  '../../redux/actions/modal';


afterEach(cleanup)

const mockStore = configureStore([]);
const store = mockStore({
    modal: false
});

test('snapshot Modal', () => {
  const { container } = render(
    <Provider store={store}>
      <Modal  />
    </Provider>
  );
  expect(container).toMatchSnapshot();

});

test('close Modal', () => {
  store.dispatch = jest.fn();
    render(
    <Provider store={store}>
      <Modal />
    </Provider>
  );
  const closeModal = screen.getByTestId('close-modal');
  fireEvent.click(closeModal);
    expect(store.dispatch).toHaveBeenCalledWith(toggleModal(null));

});

